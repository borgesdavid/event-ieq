from django.contrib import admin
from .models import Evento, Inscricao


class EventoAdmin(admin.ModelAdmin):
    list_display = ('nome','descricao','data_inicial', 'quantidade', 'ativo')

class InscricaoAdmin(admin.ModelAdmin):
    list_display = ('nome' , 'sobrenome', 'telefone', 'email', 'evento')
    list_filter = ['evento']
    search_fields = ['nome', 'sobrenome']

admin.site.register(Evento, EventoAdmin)
admin.site.register(Inscricao, InscricaoAdmin)

class StackedItemInline(admin.StackedInline):
    classes = ('grp-collapse grp-open',)

class TabularItemInline(admin.TabularInline):
    classes = ('grp-collapse grp-open',)

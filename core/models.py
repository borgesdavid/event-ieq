from django.db import models


class Evento(models.Model):
    foto = models.ImageField(upload_to='foto_evento', null=True, blank=True)
    nome = models.CharField('Nome',max_length=100)
    descricao = models.TextField('Descrição do Evento', null=True, blank=True)
    data_inicial = models.DateTimeField('Entrada', auto_now_add=False)
    data_final = models.DateTimeField('Saída',auto_now_add=False, null=True)
    local = models.CharField('Endereço', max_length=100)
    quantidade = models.IntegerField('Quantidade')
    ativo = models.BooleanField('Ativo', default=True)  

    def __str__(self):
         return f'  {self.nome} '

class Inscricao(models.Model):
    nome = models.CharField('Nome',max_length=100 )
    sobrenome = models.CharField('Sobrenome', max_length=100)
    telefone = models.CharField('Telefone', max_length=16, null=True, blank=True)
    email = models.EmailField('Email', max_length=100, unique=True)
    evento = models.ForeignKey(Evento, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.nome} - {self.sobrenome} - {self.telefone} - {self.email} - {self.evento}'

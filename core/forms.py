from django.forms import ModelForm
from .models import Evento, Inscricao

class EventoForm(ModelForm):
    class Meta:
        model = Evento
        fields = ['nome', 'descricao', 'data_inicial', 'data_final', 'local']

class InscricaoForm(ModelForm):
    class Meta:
        model = Inscricao
        fields = ['nome', 'sobrenome', 'telefone', 'email', 'evento']